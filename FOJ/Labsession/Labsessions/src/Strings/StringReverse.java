package Strings;

public class StringReverse {

	public static void main(String[] args) {
		
		String s1 = "My@Name%is(Lavanya";
		System.out.println(s1);
		s1 = s1.replaceAll("[^a-zA-Z0-9]", " ");
		String[] s2 = s1.split(" ");
		for(int i = s2.length-1; i >= 0; i--){
			System.out.print(s2[i]+" ");
		}
		
	}

}
