package Strings;

public class wordsNumber {

	 public static int maxWordsInSingleSentence(String[] sentences) {
	        int maxWords = 0;
	            for (String sentence : sentences) {
	                String[] words = sentence.split(" "); 
	                int numWords = words.length; 
	                maxWords = Math.max(maxWords, numWords); 
	            }
	            return maxWords;
	        }

	        public static void main(String[] args) {
	            String[] sentences1 = {"alice and bob love leetcode", "i think so too", "this is great thanks very much"};
	            String[] sentences2 = {"please wait", "continue to fight", "continue to win"};

	            System.out.println(maxWordsInSingleSentence(sentences1)); 
	            System.out.println(maxWordsInSingleSentence(sentences2)); 
	        }
	       

}

