package day01;

import java.util.Scanner;

public class ArrayDemo {

	public static void main(String[] args) {
		
				Scanner sc = new Scanner(System.in);
				System.out.println("enter the size of array");
				int size = sc.nextInt(); 
				int[] arr = new int[size];
				System.out.println("enter the element of an array");
				for(int i = 0;i<size;i++){
					arr[i] = sc.nextInt();
				}
				findEven(arr);
			}

			private static void findEven(int[] arr) {
				int even =0;
				for(int i=0;i<arr.length;i++){
					if(arr[i]%2==0){
						even++;
					}
				}
				System.out.println("total even counts"+even);
				
				
			}
		

	}
