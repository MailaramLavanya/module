package day01;

import java.util.Scanner;
public class LinearSearch {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size and element you want to insert in an existing array");

		int size = sc.nextInt();
		int[] arr = {2,3,1,15,4,0,0}; //
		int pos = sc.nextInt();
		int element = sc.nextInt();
		for(int i = size; i > pos - 1; i--) {
			arr[i] = arr[i - 1];
		}
		arr[pos - 1] = element;
		for(int i = 0; i <= size; i++) {
			System.out.print(arr[i] + " ");
		}
		
	}

}
