package day07;

public class AdamNumner {
	
	public static int reverse(int num) {
		int rev = 0;
		while (num > 0) {
			int rem = num % 10;
			rev = rev * 10 + rem;
			num /= 10;
		}
		return rev;
	}
	
	public static int calculateSquare(int num) {
		int sq = num * num;
		int rev = reverse(sq); 
		if (sq == rev) {
			return num;
		} else {
			return rev;
		}
	}

	public static void main(String[] args) {
		int num = 21;
		int result = calculateSquare(num);
		System.out.println("Result : " + result);
	}
}
