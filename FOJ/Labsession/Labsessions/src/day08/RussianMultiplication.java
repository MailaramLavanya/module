package day08;

public class RussianMultiplication {

	public static void main(String[] args) {
		int num1 = 11;
		int num2 = 15;
		
		int product = 0;
		
		while(num1 > 0){
			if(num1 % 2 != 0){
			
				product += num2;
				
			}
			num1 /= 2;
			num2 *= 2;
			
		}
		System.out.println(product);
		}
	

}
