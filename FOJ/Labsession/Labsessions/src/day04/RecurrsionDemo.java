package day04;

public class RecurrsionDemo {

	public static int fib(int N) {
		if(N <= 1) {
			return N;
		}
		return fib(N- 1) + fib (N - 2);
	}
	
	public static void main(String[] args) {
		int n1 = 23;
		int sum = 0;
		for(int i = 0; i <= n1; i++) {
			sum += fib(i);
		}
		System.out.print(sum + " ");
	}

}
