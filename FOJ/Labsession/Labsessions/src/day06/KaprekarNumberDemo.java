package day06;

public class KaprekarNumberDemo {
	
		public static boolean findKaprekarNumber( int num ) {

			int square = num * num;
			int sq = square;
			int count = 0;
			int sum = 0;
			while(square > 0){
				count++;
				square /= 10;
			}
			for( int i = 1 ; i < count; i++){
				int div_part = (int)Math.pow(10, i);
				sum = sq / div_part + sq % div_part;
				if(sum == num){
					return true;
				}
			}
			return false;

		}


		public static void main(String[] args) {
	        
			int num = 9;
			System.out.println(findKaprekarNumber(num));;

		}

	}

