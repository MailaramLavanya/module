package May23;
import java.util.ArrayList;
import java.util.List;

public class Replacing {
	public static String replacePlusWithMinus(String input) {
		List<Character> charList = new ArrayList<>();
		for (char c : input.toCharArray()) {
			charList.add(c);
		}
		for (int i = 0; i < charList.size() - 1; i++) {
			if (charList.get(i) == '+' && charList.get(i + 1) == '+') {
				charList.set(i, '-');
				charList.set(i + 1, '-');
			}
		}
		StringBuilder result = new StringBuilder();
		for (char c : charList) {
			result.append(c);
		}

		return result.toString();
	}

	public static void main(String[] args) {
		String input = "--++++";
		System.out.println("Original string: " + input);
		String result = replacePlusWithMinus(input);
		System.out.println("Modified string: " + result);
	}
}
