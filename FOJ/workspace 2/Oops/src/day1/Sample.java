package day1;

public class Sample {
	    
	private int a,b; //instance variable

		public Sample(){
			System.out.println("Hi! This is constructor");
		}
		public Sample(int a,int b){
			System.out.println("Hi! this is parameterized constructor");
			this.a = a;
			this.b = b;
		}
		public void read(int a,int b){
			this.a = a;
			this.b = b;
		}
		public void show(){
			System.out.println(a);
			System.out.println(b);

		}
		public void sum(){
			System.out.println("The sum is:"+(a+b));
		}

		public static void main(String[] args){
			Sample obj1 = new Sample();
			obj1.read(10,20);
			obj1.show();
			obj1.sum();


			Sample obj2 = new Sample(100, 200);
			obj2.show();
			obj2.sum();

		}

}
