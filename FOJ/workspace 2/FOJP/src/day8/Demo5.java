package day8;

public class Demo5 {
	
	public static int greatestOf2(int num1, int num2) {
		return (num1 > num2) ? num1 : num2;
	}
								
	public static int greatestOf3(int num1, int num2, int num3) {
		return (num1 > num2) ? 
				((num1 > num3) ? num1 : num3) : 
					((num2 > num3) ? num2 : num3);
	}
	
	
	public static void main(String[] args) {		
		System.out.println(greatestOf2(2, 3));		//3
		System.out.println(greatestOf2(4, 3));		//4
		
		System.out.println(greatestOf3(4, 5, 6));	//6
		System.out.println(greatestOf3(6, 5, 4));	//6
		System.out.println(greatestOf3(6, 7, 4));	//7		
	}


}
