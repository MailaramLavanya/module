package day8;

public class Demo9 {
	public static void main(String[] args) {
		
		int number = 17;
		
		while (number != 1) {
			System.out.println(number + " ");
			
			if (number % 2 == 0) {
				number /= 2;
			} else {
				number = 3 * number + 1;
			}
		}
		
		System.out.println(1);
	}

}
