package day8;

public class Demo7 {
	public static void main(String[] args) {
		// Replace this with the number you want to check
		int number = 31;

		if (isAdamNumber(number)) {
			System.out.println(number + " is an Adam number.");
		} else {
			System.out.println(number + " is not an Adam number.");
		}
	}

	public static boolean isAdamNumber(int num) {
		int square = num * num;
		int reversedSquare = reverse(square);
		int reversedNum = reverse(num);
		int squareOfReversed = reversedNum * reversedNum;

		return reversedSquare == squareOfReversed;
	}

	public static int reverse(int num) {
		int reversed = 0;
		while (num != 0) {
			int digit = num % 10;
			reversed = reversed * 10 + digit;
			num /= 10;
		}
		return reversed;
	}

}
