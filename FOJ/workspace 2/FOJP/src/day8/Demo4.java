package day8;

public class Demo4 {
	
public static int powerOf2(int num, int pow) {
		
		int result = 1;
		
		for (int i = 0; i < pow; i++) {
			result *= num;
		}
		
		return result;
	}
	
	public static void main(String[] args) {
		
		System.out.println(powerOf2(2, 3));	//8
		System.out.println(powerOf2(3, 4));	//81 
		System.out.println(powerOf2(4, 5));	//1024		
	}


}
