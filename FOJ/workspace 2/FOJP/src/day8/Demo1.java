package day8;

public class Demo1 {
	public static void showArray(int arr[]) {
		for(int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}
	}
	
	public static void main(String[] args) {
		int arr[] = {30, 10, 50, 20, 40};
		showArray(arr);		
	}


}
