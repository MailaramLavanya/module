package day7;

public class Demo6 {

	public static int getSum(int arr[]) {
		int sum = 0;
		
		for (int i = 0; i < arr.length; i++) {
			sum += arr[i];
		}
		
		return sum;
	}
	
	public static long getProduct(int arr[]) {
		long prod = 1;
		
		for (int i = 0; i < arr.length; i++) {
			prod *= arr[i];
		}
		
		return prod;
	}
	
	public static void main(String[] args) {
		
		int arr[] = {10, 20, 30, 40, 50};		
		
		System.out.println("Sum = " + getSum(arr));
		System.out.println("Prod= " + getProduct(arr));
	}

}
