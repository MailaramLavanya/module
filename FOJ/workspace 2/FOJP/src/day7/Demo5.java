package day7;

public class Demo5 {
public static void main(String[] args) {
		
		int arr[] = {10, 20, 30, 40, 50};	
		int arrLength = arr.length;
		int sum = 0;
		long product = 1;
		
		for (int i = 0; i < arrLength; i++) {
			sum += arr[i];
			product *= arr[i];
		}
		
		System.out.println("Sum     = " + sum);
		System.out.println("product = " + product);
	}


}
