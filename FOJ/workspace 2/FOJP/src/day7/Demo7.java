package day7;

public class Demo7 {
	public static void main(String[] args) {		
		int arr[] = {10, 20, 30, 40, 50};	
		int arrLength = arr.length;
		int min = arr[0];
		int max = arr[0];
		
		for (int i = 0; i < arrLength; i++) {
			
			if (min > arr[i])
				min = arr[i];
			
			if (max < arr[i])
				max = arr[i];			
		}
		
		System.out.println("Min = " + min);
		System.out.println("Max = " + max);

	}

}
