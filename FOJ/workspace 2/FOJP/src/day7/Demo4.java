package day7;

public class Demo4 {
public static void main(String[] args) {
		
		int arr[] = {10, 20, 30, 40, 50};	
		int arrLength = arr.length;
		
		System.out.println("Size of arr: " + arrLength);
		System.out.println();

		for (int i = 0; i < arrLength; i++) {
			System.out.println("arr[" + i + "] = " + arr[i]);
		}
		
	}


}
