package day11;

public class Demo6 {
	
	public static void findRowSum(int arr[][]) {					
		int sum;
		//10 20	30	60  or	Row-1 Sum = 60
		
		for (int i = 0; i < 3; i++) {
			
			sum = 0;
			for (int j = 0; j < 3; j++) {
				sum += arr[i][j];
				
				System.out.print(arr[i][j] + " ");
			}
			System.out.println(sum);
		}
		System.out.println();
		
	}
	
	public static void main(String[] args) {
		
		int arr1[][] = new int[][]{{10, 20, 30}, {40, 51, 60}, {70, 80, 91}};				
		int arr2[][] = new int[][]{{11, 12, 13}, {14, 15, 16}, {17, 18, 19}};						
		int arr3[][] = new int[][]{{21, 20, 30}, {30, 41, 50}, {60, 70, 81}};
				
		findRowSum(arr1);
		findRowSum(arr2);
		findRowSum(arr3);	
	}


}
