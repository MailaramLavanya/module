package Day5;

public class Product {
	
	public static int product(int num){
		if(num == 1)
			return 1;
		return num * product(num - 1);
		}

	public static void main(String[] args) {
		System.out.println(product(5));
		System.out.println(product(10));

	}

}
