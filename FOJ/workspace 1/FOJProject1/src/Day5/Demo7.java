package Day5;

public class Demo7 {

	public static boolean isPrime(int num){
		for(int i = 2; i < num; i++){
			if(num % i == 0){
				return false;
			}
		}
		return true;
	}
	public static void main(String[] args){
		System.out.println(isPrime(7));
		System.out.println(isPrime(23));
		System.out.println(isPrime(121));
		System.out.println(isPrime(11));
	}
}


