package Practice;

//unary
public class Unary {
	
	public static void main(String[] args) {
		int i = 10;
		
		System.out.println(i);
		System.out.println(++i);
		System.out.println(i++);
		System.out.println(i);
		
		System.out.println();

		System.out.println(i); //2
		System.out.println(--i); //1
		System.out.println(i--); //1
		System.out.println(i); //0
		
	}

}
