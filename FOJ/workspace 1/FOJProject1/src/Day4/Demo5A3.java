package Day4;

import java.text.DecimalFormat;

public class Demo5A3 {

	public static void main(String[] args) {
		
		double number = 12.333333333 ;
		DecimalFormat digit = new DecimalFormat("#.##");
		String rounded = digit.format(number);
		
		System.out.println(rounded);

	}

}
