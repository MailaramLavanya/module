package Day4;

public class Demo2 {
	
     public static boolean isLeapYear(int Year){
    	 
    	 if((Year % 400 == 0) || (Year % 100 != 0 && Year % 4 == 0)){
    		 return true;
    	 }
    	 else{
    	 
    	 return false;
     }
    	 
     }
	
     public static void main(String[] args) {
    	 
    	 System.out.println(isLeapYear(1600));
    	 System.out.println(isLeapYear(1700));
    	 System.out.println(isLeapYear(1800));
    	 System.out.println(isLeapYear(1900));
    	 System.out.println(isLeapYear(2000));
    	 System.out.println(isLeapYear(2020));
    	 System.out.println(isLeapYear(2020));
    	 System.out.println(isLeapYear(2024));
    	 
    	 
     }

}
