package day02;

public class Demo12 {

    public static int findNextMultiple(int number) {
        return number + (number % 100 == 0 ? 0 : 100 - (number % 100));
    }

    public static void main(String[] args) {
        int num = 123;
        int nextMultiple = findNextMultiple(num);
        System.out.println("The next multiple of 100 for " + num + " is: " + nextMultiple);
    }
}