package day02;

//Type casting
public class Demo6 {

	public static void main(String[] args) {
		
		String s = "25";
		byte b = Byte.parseByte(s);   //string to byte
		short sh = Short.parseShort(s);  //string to short
		int i = Integer.parseInt(s);  //string to integer
		long l = Long.parseLong(s);  //string to long
		
		//Lower to Higher
		
		System.out.println("String s : " + s);
		System.out.println("Byte b   : " + s);
		System.out.println("Short sh : " + s);
		System.out.println("int i    : " + s);
		System.out.println("long l   : " + s);
		System.out.println();
		
		l  = 45;
		i  =  (int) l;
		sh =(short) i;
		b  = (byte) sh;
		
		s = b + "";
		
		//Higher to lower
		System.out.println("long l   : " + s);
		System.out.println("int i    : " + s);
		System.out.println("Short sh : " + s);
		System.out.println("Byte b   : " + s);
		System.out.println("string s : " + s);
		System.out.println();
		
		
	
	}

}
