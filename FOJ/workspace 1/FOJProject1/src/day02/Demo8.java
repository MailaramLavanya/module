package day02;

public class Demo8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double celsius = 25.0;
		double fahrenheit = 77.0;
		
		//Converting Celsius to Fahrenheit
		double fahrenheitFromCelsius = (celsius * 9.0 / 5.0) + 32.0;
		
		
		//Converting from  Fahrenheit to Celsius
		double celsiusFromFahrenheit = (fahrenheit -32.0) * 5.0 / 9.0;
		
		System.out.println(celsius + "Degrees Celsius is equal to " + fahrenheitFromCelsius + "Degrees Fahrenheit");
		System.out.println(celsius + "Degrees Fahrenheit is equal to " + celsiusFromFahrenheit + "Degrees Celsius");


	}

}
