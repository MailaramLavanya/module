package day02;

public class Demo4 {

	public static void main(String[] args){
		
		int num1 = 10;
		int num2 = 0;
		System.out.println("num1 = " + num1 + "\nnum2 = " + num2 + "\n");
		
		System.out.println("performing pre increment and decrement");
		System.out.println("--------------------------------------");
		
		num2 = ++num1;
		System.out.println("num2 = ++num1");
		System.out.println("num1 = " + num1 + "\num2 = " + num2 + "\n");
		
		num2 = --num1;
		System.out.println("num2 = ++num1");
		System.out.println("num1 = " + num1 + "\num2 = " + num2 + "\n");
		
		System.out.println("performing post increment and decrement");
		System.out.println("--------------------------------------");
		
		num2 = num1++;
		System.out.println("num2 = num1++");
		System.out.println("num1 = " + num1 + "\num2 = " + num2 + "\n");
		
		num2 = num1--;
		System.out.println("num2 = num1--");
		System.out.println("num1 = " + num1 + "\num2 = " + num2 + "\n");
		
		
		
	}
}
