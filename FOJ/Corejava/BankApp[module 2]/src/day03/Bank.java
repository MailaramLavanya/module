package day03;

public class Bank {
	
	int index = 0;
	Customer customer[] = new Customer[10];
	
	
	public void addCustomer(Customer customer) {
		this.customer[index] = customer;
		index++;
		
		System.out.println("Customer Added Successfully!!! \n");
	}
	
	public void showCustomers() {
		if (index > 0) {
			System.out.println("Customers Info \n");
			
			for (int i = 0; i < index; i++) {
				System.out.println(customer[i]);
			}

			System.out.println();
		} else {
			System.out.println("No Customers Found!!!");
		}
	}

	public void showCustomerById(int customerId) {
		
		Customer cust = null;
		
		for (int i = 0; i < index; i++) {
			if (customer[i].getCustomerId() == customerId) {
				cust = customer[i];
				break;
			}
		}
		
		if (cust != null) {
			System.out.println(cust);
		} else {
			System.out.println("Customer with " + customerId + ", Not Found!!!");
		}		
	}
	
	public boolean isCustomerExists(int customerId) {		
		for (int i = 0; i < index; i++) {
			if (customer[i].getCustomerId() == customerId) {
				return true;
			}
		}		
		return false;
	}
	
	public Customer getCustomer(int customerId) {
		for (int i = 0; i < index; i++) {
			if (customer[i].getCustomerId() == customerId) {
				return customer[i];
			}
		}		
		return null;
	}

	public void deposite(int customerId, double amount) {
		Customer customer = getCustomer(customerId);
		
		if (customer != null) {
			customer.setBalance(customer.getBalance() + amount);
			System.out.println("Amount Deposited: " + amount);
			System.out.println("New Balance     : " + customer.getBalance());
			System.out.println();
		} else {
			System.out.println("Please Provide the Valid Customer Id");
		}
	}

	public void withdraw(int customerId, double amount) {
		
	}

}