package day03;
	
	class Calc {
		public void arithDemo(int num1, int num2) throws ArithmeticException, Exception {
			System.out.println("Sum = " + (num1 +  num2));
			System.out.println("Sub = " + (num1 -  num2));
			System.out.println("Mul = " + (num1 *  num2));
			System.out.println("Quo = " + (num1 /  num2));
			System.out.println("Rem = " + (num1 %  num2));
			System.out.println("Done!! \n\n");
		}
	}


	public class Demo1 {
		public static void main(String[] args) {
			Calc obj = new Calc();
			
			try {
				obj.arithDemo(10, 0);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			
			try {
				obj.arithDemo(10, 3);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}

}
