package day03;
	
	class Parent1 {
		public void m1() {
			System.out.println("M1() from Parent Class");
		}
	}

	public class Inheritance extends Parent1 {
		public static void main(String[] args) {

			Inheritance obj = new Inheritance();
			obj.m1();
			
		}
	}

