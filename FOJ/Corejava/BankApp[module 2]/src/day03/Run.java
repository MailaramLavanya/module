package day03;

import java.util.Scanner;

public class Run {
	
	public static void main(String[] args) {
		int customerId = 0;
		double amount = 0.0;
		Bank bank = new Bank();
		Scanner scan = new Scanner(System.in);
				
		while (true) {
			System.out.println("\n");
			System.out.println("Select Your Choice");
			System.out.println("******************");
			System.out.println("1. Add Customer");
			System.out.println("2. Show Customers");
			System.out.println("3. Show Customer By Id");
						System.out.println("4. Deposit");
			System.out.println("5. WithDraw");
			System.out.println("6. Check Balance");
			System.out.println("7. Fund Transfer");
			System.out.println("8. Exit \n");
			
			System.out.print("Enter Your Choice: ");
			int choice = scan.nextInt();
			System.out.println();
			
			switch (choice) {
			case 1:
				System.out.print("Enter First Name: ");
				String firstName = scan.next();
				
				System.out.print("Enter Last  Name: ");
				String lastName = scan.next();
				
				System.out.print("Enter Address   : ");
				String address = scan.next();
				
				System.out.print("Enter Balance   : ");
				double balance = scan.nextDouble();
				
				Customer customer = new Customer(firstName, lastName, address, balance);
				bank.addCustomer(customer);
				
			break;
			
			case 2:bank.showCustomers();
			break;
			
			case 3:
				System.out.print("Enter Customer ID: ");
				customerId = scan.nextInt();
				bank.showCustomerById(customerId);
			break;
			
			case 4://Deposite
				System.out.print("Enter Customer ID: ");
				customerId = scan.nextInt();
				System.out.print("Enter Amount     : ");
				amount = scan.nextDouble();
				bank.deposite(customerId, amount);
				
			break;
			
			case 5://Withdraw
				System.out.print("Enter Customer ID: ");
				customerId = scan.nextInt();
				System.out.print("Enter Amount     : ");
				amount = scan.nextDouble();
				bank.withdraw(customerId, amount);
			break;
			
			case 8: System.out.println("Thank You.");
					System.out.println("Application Terminated...\n");
					System.exit(0);
			break;
			
			default: System.out.println("Invalid Chioce \n");
			break;
			}
		}
		
		
	}

}
