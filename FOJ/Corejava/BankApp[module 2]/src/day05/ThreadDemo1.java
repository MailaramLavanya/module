package day05;

class Demo extends Thread {
	public void run() {
		System.out.println("Thread Executed...");
	}
}

public class ThreadDemo1 {
	public static void main(String[] args) {

		Demo obj1 = new Demo();	//Created Thread-1
		Demo obj2 = new Demo();	//Created Thread-2
		Demo obj3 = new Demo();	//Created Thread-3
		
		obj1.start();
		obj2.start();
		obj3.start();		
	}

}
