package day05;

class MyThread2 extends Thread {	
	
	String threadName;
	
	public MyThread2(String threadName) {
		this.threadName = threadName;
	}
	
	public void run() {
		
		for (int i = 1; i <= 3; i++) {
			System.out.println("Thread: " + this.threadName);
			
			if (this.threadName.equals("T3")) {
				System.out.println();
			}
		}
		
	}	
}

public class ThreadDemo3 {
	public static void main(String[] args) {

		MyThread2 thread1 = new MyThread2("T1");
		MyThread2 thread2 = new MyThread2("T2");
		MyThread2 thread3 = new MyThread2("T3");
		
		thread1.start();
		thread2.start();
		thread3.start();
	}

}
