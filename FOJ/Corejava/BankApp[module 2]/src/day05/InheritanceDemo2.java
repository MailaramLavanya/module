package day05;

class Parent1 {
	public void m1() {
		System.out.println("m1() from Parent1 Class");
	}
}

interface Parent2 {
	void m2();
}

interface Parent3 {
	void m3();
}

class Child2 extends Parent1 implements Parent2, Parent3 {
	public void m2() {
		System.out.println("m2() from Parent2 interface");
	}
	public void m3() {
		System.out.println("m3() from Parent3 interface");
	}
}


public class InheritanceDemo2 {
	public static void main(String[] args) {
		
		Child2 obj = new Child2();
		obj.m1();
		obj.m2();
		obj.m3();
		
	}
}

