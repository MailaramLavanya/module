package day05;

class MyThread4 extends Thread {	
	
	String threadName;
	
	public MyThread4(String threadName) {
		this.threadName = threadName;
	}
	
	public void run() {
		System.out.println("Thread: " + this.threadName);
	}	
}

public class ThreadDemo4 {
	public static void main(String[] args) {

		MyThread4 thread1 = new MyThread4("T1");
		MyThread4 thread2 = new MyThread4("T2");
		MyThread4 thread3 = new MyThread4("T3");
		
		System.out.println("Thread-1 Priority: " + thread1.getPriority());
		System.out.println("Thread-2 Priority: " + thread2.getPriority());
		System.out.println("Thread-3 Priority: " + thread3.getPriority());
		System.out.println();
		
		thread1.setPriority(10);	//High   Priority
		thread2.setPriority(5);		//Medium Priority
		thread3.setPriority(1);		//Low    Priority
		
		System.out.println("Thread-1 Priority: " + thread1.getPriority());
		System.out.println("Thread-2 Priority: " + thread2.getPriority());
		System.out.println("Thread-3 Priority: " + thread3.getPriority());
		System.out.println();
		
		thread1.start();
		thread2.start();
		thread3.start();
	}
}


