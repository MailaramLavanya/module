package day05;


interface Parent4 {
	void parentMethod();
}

class Child4 implements Parent4 {
	
	@Override
	public void parentMethod() {
		System.out.println("Abstract ParentMethod() from Parent Interface is Defined in the Child Class");
	}
	
	public void childMethod() {
		System.out.println("ChildMethod() from Child Class");
	}
}

public class InheritanceDemo3 {
	public static void main(String[] args) {
		
		System.out.println("Using Child Class Object");
		Child4 child = new Child4();
		child.childMethod();
		child.parentMethod();
		System.out.println();
		
		//parent = child;	//parent = new Child();
		Parent4 obj = new Child4();
		obj.parentMethod();
		//obj.childMethod();	//Not Accessible
	}
}

