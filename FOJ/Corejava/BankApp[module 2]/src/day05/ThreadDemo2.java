package day05;

class MyThread extends Thread {	
	
	String threadName;
	
	public MyThread(String threadName) {
		this.threadName = threadName;
	}
	
	public void run() {
		System.out.println("Thread: " + this.threadName + " Executed...");
	}	
}

public class ThreadDemo2 {
	public static void main(String[] args) {

		MyThread thread1 = new MyThread("T1");
		MyThread thread2 = new MyThread("T2");
		MyThread thread3 = new MyThread("T3");
		
		thread1.start();
		thread2.start();
		thread3.start();
	}
}

