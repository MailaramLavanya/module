package day05;

public class SynchronousDemo {
	
	public void myMethod1() {
		for (int i = 1; i <= 5; i++) {
			System.out.println("MyMethod1: " + i);
			
			try {
				Thread.sleep(2000);		//1000 = 1000 milli seconds = 1 second
			} catch (InterruptedException e) {
				e.printStackTrace();
			} 	
		}
	}
	
	public void myMethod2() {
		for (int i = 1; i <= 5; i++) {
			System.out.println("MyMethod2: " + i);
		}
	}

	public static void main(String[] args) {
		SynchronousDemo obj = new SynchronousDemo();
		obj.myMethod1();
		
		System.out.println();
		obj.myMethod2();
	}

}

