package day05;

interface Car {	
	void steering();	
	void brakes();	
	void color();
}

class Swift implements Car {
	
	@Override
	public void steering() {
		System.out.println("Steering: Power Steering");
	}
	
	@Override
	public void brakes() {
		System.out.println("Brakes  : Disk Brakes");
	}

	@Override
	public void color() {
		System.out.println("Color   : Red Color");
	}
	
	public void musicSystem() {
		System.out.println("Music   : Sony Music System");
	}
}

class Creta implements Car {
	
	@Override
	public void steering() {
		System.out.println("Steering: Power Steering");
	}
	
	@Override
	public void brakes() {
		System.out.println("Brakes  : Disk Brakes");
	}
	
	@Override
	public void color() {
		System.out.println("Color   : Maroon Color");
	}
	
	public void musicSystem() {
		System.out.println("Music   : Bose Music System");
	}

	
}

public class InheritanceDemo1 {
	public static void main(String[] args) {
		
		System.out.println("Swift Car Specifications");
		System.out.println("------------------------");
		Swift swift = new Swift();
		swift.color();
		swift.brakes();
		swift.steering();
		swift.musicSystem();
		System.out.println();
		
		
		System.out.println("Creta Car Specifications");
		System.out.println("------------------------");
		Creta creta = new Creta();
		creta.color();
		creta.brakes();
		creta.steering();
		creta.musicSystem();
		System.out.println();	
	}
}





