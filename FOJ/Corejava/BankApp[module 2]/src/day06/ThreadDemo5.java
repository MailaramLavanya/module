package day06;

class MyThread5 extends Thread {	
	
	String threadName;
	
	public MyThread5(String threadName) {
		this.threadName = threadName;
	}
	
	public void run() {
		for (int i = 1; i <= 3; i++) {
			System.out.println("Thread: " + this.threadName);
			
			if (this.threadName.equals("T3")) {
				System.out.println();
				
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}	
}

public class ThreadDemo5 {
	public static void main(String[] args) {

		MyThread5 thread1 = new MyThread5("T1");
		MyThread5 thread2 = new MyThread5("T2");
		MyThread5 thread3 = new MyThread5("T3");
				
		thread1.start();
		thread2.start();
		thread3.start();
		
		try {
			thread1.join();
			thread2.join();
			thread3.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println("Program Terminated...");
	}
}
