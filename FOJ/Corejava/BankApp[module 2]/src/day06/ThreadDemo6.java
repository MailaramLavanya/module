//Thread class using runnable interface


package day06;

class MyThread6 implements Runnable {
	
	String threadName;
	
	MyThread6(String threadName) {
		this.threadName = threadName;
	}

	@Override
	public void run() {
		System.out.println(this.threadName + " Executed...");
	}	
}

public class ThreadDemo6 {
	public static void main(String[] args) {
		MyThread6 myThread1 = new MyThread6("T1");
		MyThread6 myThread2 = new MyThread6("T2");
		MyThread6 myThread3 = new MyThread6("T3");
		
		Thread thread1 = new Thread(myThread1);
		Thread thread2 = new Thread(myThread2);
		Thread thread3 = new Thread(myThread3);
		
		thread1.start();
		thread2.start();
		thread3.start();
	}
}


