package day06;

class MyThread7 implements Runnable {

	@Override
	public void run() {
		System.out.println("Thread Executed...");
	}	
}

public class ThreadDemo7 {
	public static void main(String[] args) {
		MyThread7 myThread = new MyThread7();
		
		Thread thread = new Thread(myThread);
		thread.start();
	}
}
