package day04;

public class Commercialcustomer extends Customer {
	
		private String contactPersonName;
		private long contactPersonNumber;
		
		public Commercialcustomer() {
			super();
		}
		
		public Commercialcustomer(String firstName, String lastName, String address, double balance, String contactPersonName, long contactPersonNumber) {
			super(firstName, lastName, address, balance);
			
			this.contactPersonName = contactPersonName;
			this.contactPersonNumber = contactPersonNumber;
		}

		public String getContactPersonName() {
			return contactPersonName;
		}

		public void setContactPersonName(String contactPersonName) {
			this.contactPersonName = contactPersonName;
		}

		public long getContactPersonNumber() {
			return contactPersonNumber;
		}

		public void setContactPersonNumber(long contactPersonNumber) {
			this.contactPersonNumber = contactPersonNumber;
		}

		@Override
		public String toString() {
			return "CommercialCustomer [customerId=" + customerId +
					", firstName=" + firstName + ", lastName=" + lastName + 
					", address=" + address +  ", balance=" + balance + 
					", contactPersonNumber=" + contactPersonNumber + 
					", contactPersonName=" + contactPersonName + "]";
		}
	}

