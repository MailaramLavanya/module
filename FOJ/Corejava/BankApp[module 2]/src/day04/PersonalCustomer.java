package day04;

public class PersonalCustomer extends Customer {
	
		private long officePhone;
		private long homePhone;
		
		public PersonalCustomer() {
			super();
		}
		
		public PersonalCustomer(String firstName, String lastName, String address, double balance, long officePhone, long homePhone) {
			super(firstName, lastName, address, balance);
			
			this.officePhone = officePhone;
			this.homePhone = homePhone;
		}

		public long getOfficePhone() {
			return officePhone;
		}

		public void setOfficePhone(long officePhone) {
			this.officePhone = officePhone;
		}

		public long getHomePhone() {
			return homePhone;
		}

		public void setHomePhone(long homePhone) {
			this.homePhone = homePhone;
		}

		@Override
		public String toString() {
			return "PersonalCustomer [customerId=" + customerId + 
					", firstName=" + firstName + ", lastName=" + lastName + 
					", address=" + address + ", balance=" + balance + 
					", officePhone=" + officePhone + 
					", homePhone=" + homePhone + "]";
		}
		
		
	}


