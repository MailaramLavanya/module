package day07;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class IteratorDemo1 {

	public static void main(String[] args) {

		Set<Integer> set = new HashSet<Integer>();

		set.add(10);
		set.add(20);
		set.add(30);
		set.add(40);
		set.add(50);

		Iterator it = set.iterator();
		while (it.hasNext()) {
			System.out.println(it.next());
		}

	}

}
