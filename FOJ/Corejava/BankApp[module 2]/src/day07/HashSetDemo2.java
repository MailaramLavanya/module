package day07;

import java.util.HashSet;
import java.util.Set;

public class HashSetDemo2 {
	public static void main(String[] args) {
		
		Set<Integer> set = new HashSet<Integer>();
		
		set.add(10);
		set.add(20);
		set.add(30);
		set.add(10);
		set.add(20);
		
		System.out.println(set);
		
		//foreach or enhanced for loop
		for (Object obj : set) {
			System.out.println(obj);
		}
	}
}
