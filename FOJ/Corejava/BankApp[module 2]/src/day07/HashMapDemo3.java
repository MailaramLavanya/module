package day07;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class HashMapDemo3 {
	public static void main(String[] args) {

		Map<String, Integer> map = new HashMap<String, Integer>();
		
		map.put("Sachin", 75);
		map.put("Kholi",  85);
		map.put("Dhoni",  95);
		map.put("Rohith", 65);
		map.put("Sachin", 85);
		
		System.out.println(map);
		System.out.println("Sachin: " + map.get("Sachin"));
		System.out.println();
		
		Set<String> keys = map.keySet();
		Iterator it = keys.iterator();
		String key = "";
		while (it.hasNext()) {
			key = (String) it.next();
			System.out.println(key + ": " + map.get(key));
		}
		System.out.println();
		
		Collection<Integer> values = map.values();
		System.out.println(values);
		
		for (Integer i : values) {
			System.out.println(i);
		}
	}
}



