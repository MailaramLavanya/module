package day07;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Map;

public class HashTableDemo1 {

	public static void main(String[] args) {
		Hashtable<String, Integer> map = new Hashtable<String, Integer>();

		map.put("Sachin", 75);
		map.put("Kholi",  85);
		map.put("Dhoni",  95);
		map.put("Rohith", 65);
		map.put("Sachin", 85);
		System.out.println(map);
		
		System.out.println("Sachin: " + map.get("Sachin"));
		
		Enumeration<String> enumeration = map.keys();
		String key = "";
		while (enumeration.hasMoreElements()) {
			//System.out.println(enumeration.nextElement());
			key = enumeration.nextElement();
			System.out.println(key + ": " + map.get(key));
		}
				
	}

}

