package day07;

import java.util.HashMap;
import java.util.Map;

public class HashMapDemo1 {
	public static void main(String[] args) {

		Map<String, Integer> map = new HashMap<String, Integer>();
		
		map.put("Sachin", 75);
		map.put("Kholi",  85);
		map.put("Dhoni",  95);
		map.put("Rohith", 65);
		map.put("Sachin", 85);
		
		System.out.println(map);		
	}
}

