package day07;

import java.util.HashSet;
import java.util.Set;

public class HashSetDemo1 {
	public static void main(String[] args) {
		
		Set set = new HashSet();
		
		set.add(10);
		set.add("Hello");
		set.add(true);
		set.add(10);
		set.add(true);
		
		System.out.println(set);
		
		//foreach or enhanced for loop
		for (Object obj : set) {
			System.out.println(obj);
		}
	}
}
