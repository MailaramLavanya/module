//Araylist using foreach loop

package day07;

import java.util.ArrayList;
import java.util.List;

public class ArrayListDemo1 {
	public static void main(String[] args) {
		
		List al = new ArrayList();		
		al.add(10);
		al.add(20);
		al.add(30);
		System.out.println(al);
		
		
		//foreach or enhanced for loop
		for (Object obj : al) {
			System.out.println(obj);
		}
		
	}
}
