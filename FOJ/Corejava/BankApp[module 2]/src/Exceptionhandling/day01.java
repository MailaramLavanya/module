package Exceptionhandling;

public class day01 {

	public static void main(String[] args) {
		
		int num1 = 10;
		int num2 = 5;
		int[] arr = new int[4];
		
		System.out.println("program started");
		System.out.println("sum : " + (num1 + num2));
		try {
			
			System.out.println("Diff : " + (num1 - num2));
			System.out.println("Multiply : " + (num1 * num2));
			System.out.println("Divide : " + (num1 / num2));
		}catch(ArithmeticException e){
			System.out.println(e.getMessage());
		}
		catch(ArrayIndexOutOfBoundsException ae) {
			System.out.println(ae.getMessage());
			
		}
		finally{
			System.out.println("Finally block");
		}
		
	}

}
