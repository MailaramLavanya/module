package day01;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JDBCDemo1 {

	public static void main(String[] args) {
		
String url = "jdbc:mysql://localhost:3306/fsd63";
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver"); //Type 4 Driver
			
			Connection con = DriverManager.getConnection(url, "root", "root");
			
			if (con != null) {
				System.out.println("Connection Established Successfully!!!");
				con.close();
			} else {
				System.out.println("Unable to Establish the Connection!!");
			}
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}

	}

}
