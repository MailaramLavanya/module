package day03;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

import com.db.DbConnection;

public class UpdateDemo1 {
	public static void main(String[] args) {

		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		
		Scanner scan = new Scanner(System.in);		
		System.out.print("Enter EmpId : ");
		int empId = scan.nextInt();
		System.out.print("Enter Salary: ");
		double salary = scan.nextDouble();
		System.out.println();
		
		String updateQry = "update employee set salary = ? where empId = ?";
		
		try {
			pst = con.prepareStatement(updateQry);
			pst.setDouble(1, salary);
			pst.setInt(2, empId);			
			int result = pst.executeUpdate();
			
			if (result > 0) {
				System.out.println("Record Updated into the Table");
			} else {
				System.out.println("Failed to Update the Record");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if (con != null) {
					pst.close();
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}		
	}
}



