package day03;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

import com.db.DbConnection;

public class DeleteDemo {
	public static void main(String[] args) {

		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		
		Scanner scan = new Scanner(System.in);		
		System.out.print("Enter EmpId : ");
		int empId = scan.nextInt();
		System.out.println();
		
		String deleteQry = "delete from employee where empId = ?";
		
		try {
			pst = con.prepareStatement(deleteQry);
			pst.setInt(1, empId);			
			int result = pst.executeUpdate();
			
			if (result > 0) {
				System.out.println("Record Deleted into the Table");
			} else {
				System.out.println("Failed to Delete the Record");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if (con != null) {
					pst.close();
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}		
	}
}











