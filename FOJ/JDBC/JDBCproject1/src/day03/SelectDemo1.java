package day03;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.db.DbConnection;

public class SelectDemo1 {
	public static void main(String[] args) {
		
		Connection con = DbConnection.getConnection();
		Statement pst = null;
		
		if (con == null) {
			System.out.println("Unable to Establish the Connection");
			return;
		}
		
		try {
			pst = con.createStatement();
			ResultSet rs = pst.executeQuery("Select * from employee");
			
			while (rs.next()) {
				System.out.print(rs.getInt(1) + " ");
				System.out.print(rs.getString(2) + " ");
				System.out.print(rs.getDouble("salary") + " ");
				System.out.print(rs.getString(4) + " ");
				System.out.print(rs.getString(5) + " ");
				System.out.println();
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if (con != null) {
					pst.close();
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}





