package com.db;

	import java.sql.Connection;
	import java.sql.DriverManager;
	import java.sql.SQLException;

	public class DbConnection {

		public static Connection getConnection() {
			String url = "jdbc:mysql://localhost:3306/fsd63";
					
			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
				return DriverManager.getConnection(url, "root", "root");
			} catch (ClassNotFoundException | SQLException e) {
				e.printStackTrace();
			}
			
			return null;
		}
		
	}


