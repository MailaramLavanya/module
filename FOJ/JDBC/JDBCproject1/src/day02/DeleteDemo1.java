package day02;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import com.db.DbConnection;

public class DeleteDemo1 {
	public static void main(String[] args) {
	
		Connection con = DbConnection.getConnection();
		Statement stmt = null;
	
		if (con == null) {
			System.out.println("Unable to Esablish the Connection");
			return;
		}
		
		Scanner scan = new Scanner(System.in);		
		System.out.print("Enter EmpId: ");
		int empId = scan.nextInt();
		System.out.print("New Salary : ");
		double salary = scan.nextDouble();
		System.out.println();
		
        String deleteQry = "delete from employee where empId = " + empId;
		
		try {
			stmt = con.createStatement();
			int result = stmt.executeUpdate(deleteQry);
			
			if (result > 0)
				System.out.println("Record(s) deleted!!!");
			else
				System.out.println("Failed to delete the Record!!!");
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if (con != null) {
					stmt.close();
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
