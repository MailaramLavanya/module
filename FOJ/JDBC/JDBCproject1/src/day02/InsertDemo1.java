package day02;
	
	import java.sql.Connection;
	import java.sql.DriverManager;
	import java.sql.SQLException;
	import java.sql.Statement;

	import com.db.DbConnection;

	public class InsertDemo1 {
		public static void main(String[] args) {
			
			Connection con = DbConnection.getConnection();		
			String url = "jdbc:mysql://localhost:3306/fsd63";
			Statement smt = null;
			
			String insertQry = "insert into employee values (106, 'Ramu', 1212.12, 'Male', 'ramu@gmail.com', '123')";
			
			
			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
				con = DriverManager.getConnection(url, "root", "root");
				
				smt = con.createStatement();			
				int result = smt.executeUpdate(insertQry);
				
				if (result > 0) {
					System.out.println("Record Inserted into the Table");
				} else {
					System.out.println("Failed to Insert the Record!!!");
				}
				
			} catch (ClassNotFoundException | SQLException e) {
				e.printStackTrace();
			}
			
			finally {
				try {
					if (con != null) {
						smt.close();
						con.close();
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			
			
		}
	}






