package day02;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import com.db.DbConnection;


//Employee Login
public class SelectDemo4 {
	public static void main(String[] args) {
		
		Connection con = DbConnection.getConnection();
		Statement stmt = null;
		
		if (con == null) {
			System.out.println("Unable to Establish the Connection");
			return;
		}
		
		Scanner scan = new Scanner(System.in);
		System.out.print("Enter Email-Id: ");
		String emailId = scan.next();
		System.out.print("Enter Password: ");
		String password = scan.next();
		System.out.println();
		
		String loginQry = "Select * from employee where emailId = '" + emailId + "' and password = '" + password + "'";
		
		try {
			stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(loginQry);
			
			if (rs.next()) {
				System.out.println("EmpId   : " + rs.getInt(1));
				System.out.println("EmpName : " + rs.getString(2));
				System.out.println("Salary  : " + rs.getDouble("salary"));
				System.out.println("Gender  : " + rs.getString(4));
				System.out.println("Email-Id: " + rs.getString(5));
				System.out.println();
			} else {
				System.out.println("Employee Record Not Found!");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if (con != null) {
					stmt.close();
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}




