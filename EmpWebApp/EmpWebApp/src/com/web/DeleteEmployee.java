package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.EmployeeDao;

@WebServlet("/DeleteEmployee")
public class DeleteEmployee extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		int empId = Integer.parseInt(request.getParameter("empId"));
		EmployeeDao empDao = new EmployeeDao();
		int result = empDao.deleteEmployeeById(empId);
		
		if (result > 0) {
			request.getRequestDispatcher("GetAllEmployees").include(request, response);
		} else {			
			request.getRequestDispatcher("HRHomePage.jsp").include(request, response);
			out.println("<center><br/>");
			out.println("<h3 style='color:red;'> Unable to Delete the Employee Record </h3>");
			out.println("</center>");
		}
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}





