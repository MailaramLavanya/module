package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/EmpHomePage")
public class EmpHomePage extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		//String emailId = request.getParameter("emailId");
		
		//HttpSession, false = use the existing session
		HttpSession session = request.getSession(false);
		String emailId = (String) session.getAttribute("emailId");
		
		out.println("<html>");
		out.println("<body bgcolor='lightyellow' text='blue'>");
	
		out.println("<h4>Welcome " + emailId + "! </h4>");
		
		out.println("<center>");
		out.println("<form align='right'>");
		out.println("<h4><a href='EmpHomePage'>Home</a> &nbsp;");
		out.println("<a href='Logout'>Logout</a></h4>");
		out.println("</form>");
		
		out.println("<h1>Welcome to EmployeeHomePage</h1>");	
		
		out.println("</center>");
		out.println("</body>");
		out.println("</html>");
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}

