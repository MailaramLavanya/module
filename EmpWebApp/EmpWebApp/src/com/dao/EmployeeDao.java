package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.db.DbConnection;
import com.dto.Employee;

public class EmployeeDao {

	public Employee empLogin(String emailId, String password) {
		
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		String loginQry = "Select * from employee where emailId=? and password=?";
		
		if (con == null) {
			return null;
		}
		
		try {
			pst = con.prepareStatement(loginQry);
			pst.setString(1, emailId);
			pst.setString(2, password);
			rs = pst.executeQuery();
			
			if (rs.next()) {
				Employee emp = new Employee();
				
				emp.setEmpId(rs.getInt(1));
				emp.setEmpName(rs.getString(2));
				emp.setSalary(rs.getDouble("salary"));
				emp.setGender(rs.getString(4));
				emp.setEmailId(rs.getString(5));
				emp.setPassword(rs.getString(6));
				
				return emp;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if (con != null) {
					rs.close();
					pst.close();
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return null;
	}

	public int employeeRegistration(Employee emp) {
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		
		String insertQry = "insert into employee " + 
		"(empName, salary, gender, emailId, password) values (?, ?, ?, ?, ?)";
		
		if (con == null) {
			return 0;
		}
		
		try {
			pst = con.prepareStatement(insertQry);
			
			pst.setString(1, emp.getEmpName());
			pst.setDouble(2, emp.getSalary());
			pst.setString(3, emp.getGender());
			pst.setString(4, emp.getEmailId());
			pst.setString(5, emp.getPassword());
					
			return pst.executeUpdate();	
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if (con != null) {
					pst.close();
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return 0;
	}

	public Employee getEmployeeById(int empId) {
		
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		String selectQry = "Select * from employee where empId=?";
		
		if (con == null) {
			return null;
		}
		
		try {
			pst = con.prepareStatement(selectQry);
			pst.setInt(1, empId);
			rs = pst.executeQuery();
			
			if (rs.next()) {
				Employee emp = new Employee();
				
				emp.setEmpId(rs.getInt(1));
				emp.setEmpName(rs.getString(2));
				emp.setSalary(rs.getDouble(3));
				emp.setGender(rs.getString(4));
				emp.setEmailId(rs.getString(5));
				emp.setPassword(rs.getString(6));
				
				return emp;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if (con != null) {
					rs.close();
					pst.close();
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return null;
	}

	public List<Employee> getAllEmployees() {
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		String selectQry = "Select * from employee";
		
		if (con == null) {
			return null;
		}
		
		try {
			pst = con.prepareStatement(selectQry);
			rs = pst.executeQuery();
			
			List<Employee> empList = new ArrayList<Employee>();
			
			while (rs.next()) {
				Employee emp = new Employee();
				
				emp.setEmpId(rs.getInt(1));
				emp.setEmpName(rs.getString(2));
				emp.setSalary(rs.getDouble(3));
				emp.setGender(rs.getString(4));
				emp.setEmailId(rs.getString(5));
				emp.setPassword(rs.getString(6));
				
				empList.add(emp);
			}
			
			return empList;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if (con != null) {
					rs.close();
					pst.close();
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return null;
	}

	public int deleteEmployeeById(int empId) {

		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		
		String deleteQry = "delete from employee where empId=?";
		
		if (con == null) {
			return 0;
		}
		
		try {
			pst = con.prepareStatement(deleteQry);
			pst.setInt(1, empId);
			
			return pst.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if (con != null) {
					pst.close();
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return 0;
	}

	public int updateEmployee(Employee emp) {
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		
		String updateQry = 
		"update employee set empName=?, salary=?, gender=?, emailId=?, password=? where empId=?";
		
		if (con == null) {
			return 0;
		}
		
		try {
			pst = con.prepareStatement(updateQry);
			
			pst.setString(1, emp.getEmpName());
			pst.setDouble(2, emp.getSalary());
			pst.setString(3, emp.getGender());
			pst.setString(4, emp.getEmailId());
			pst.setString(5, emp.getPassword());
			pst.setInt(6, emp.getEmpId());
					
			return pst.executeUpdate();	
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if (con != null) {
					pst.close();
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return 0;
	}
	
}

