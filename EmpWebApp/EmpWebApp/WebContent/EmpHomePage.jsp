<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>EmpHomePage</title>
</head>
<body bgcolor='lightyellow' text='blue'>

	<% String emailId = (String) session.getAttribute("emailId"); %>
	<h4> Welcome <%=emailId%>! </h4>

	<center>
		<form align='right'>
			<h4> 
				<a href='EmpHomePage.jsp'>Home</a> &nbsp; 
				<a href='Logout'>Logout</a> 
			</h4>
		</form>

		<h1>Welcome to EmployeeHomePage</h1>
		<h3> <a href='Profile.jsp'>Profile</a> </h3> <br />
	</center>
</body>
</html>
